# This is a cross-platform list tracking distribution packages needed for install and tests;
# see https://docs.openstack.org/infra/bindep/ for additional information.

libpq-dev [platform:dpkg test]
mysql-client [platform:dpkg test]
mysql-server [platform:dpkg test]
postgresql [test]
postgresql-client [platform:dpkg test]

libpq-devel [platform:rpm test]
mariadb [platform:rpm test]
mariadb-devel [platform:rpm test]
mariadb-server [platform:rpm test]
postgresql-devel [platform:rpm test]
postgresql-server [platform:rpm test]
